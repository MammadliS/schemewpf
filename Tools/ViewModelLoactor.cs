﻿using Autofac;
using SchemeWpf.Navigation;
using SchemeWpf.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeWpf.Tools
{
    class ViewModelLoactor
    {
        public IContainer Container { get; set; }
        INavigationService navigationService = new NavigationService();

        public MainViewModel MainViewModel { get; }
        public LoginViewModel LoginViewModel { get; }
        public LoadingViewModel LoadingViewModel { get; }
        public ProjectsListViewModel ProjectsListViewModel { get; }
        public BoardViewModel BoardViewModel { get; set; }

        public ViewModelLoactor()
        {
            try
            {
                var builder = new ContainerBuilder();
                //builder.RegisterType<MoviesRepository>().As<IMoviesRepository>().InstancePerLifetimeScope();
                builder.RegisterType<MainViewModel>();
                builder.RegisterType<LoginViewModel>();
                builder.RegisterType<LoadingViewModel>();
                builder.RegisterType<ProjectsListViewModel>();
                builder.RegisterType<BoardViewModel>();


                builder.RegisterInstance(navigationService).As<INavigationService>().SingleInstance();
                Container = builder.Build();

                using (var scope = Container.BeginLifetimeScope())
                {
                    MainViewModel = scope.Resolve<MainViewModel>();
                    LoginViewModel = scope.Resolve<LoginViewModel>();
                    LoadingViewModel = scope.Resolve<LoadingViewModel>();
                    ProjectsListViewModel = scope.Resolve<ProjectsListViewModel>();
                    BoardViewModel = scope.Resolve<BoardViewModel>();
                }

                navigationService.AddPage(LoginViewModel, VM.LoginViewModel);
                navigationService.AddPage(LoadingViewModel, VM.LoadingViewModel);
                navigationService.AddPage(ProjectsListViewModel, VM.ProjectsListViewModel);
                navigationService.AddPage(BoardViewModel, VM.BoardViewModel);

                navigationService.NavigateTo(VM.LoginViewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
