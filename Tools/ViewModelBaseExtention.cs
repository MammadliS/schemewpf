﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using SchemeWpf.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeWpf.Tools
{
    class ViewModelBaseExtention : ViewModelBase
    {
        protected INavigationService navigationService;

        public ViewModelBaseExtention(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }

        private RelayCommand<VM> navigationCommand;
        public RelayCommand<VM> NavigationCommand
        {
            get
            {
                return navigationCommand ?? (navigationCommand = new RelayCommand<VM>(
                    param =>
                    {
                        navigationService.NavigateTo(param);
                    }
                ));
            }
        }
    }
}
