﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeWpf.Models
{
    class BoardList : ObservableObject
    {

        private string name;
        public string Name
        {
            get => name;
            set => Set(ref name, value);
        }
        private ObservableCollection<Card> _cards;
        public ObservableCollection<Card> Cards { get => _cards; set => Set(ref _cards, value); }
    }
}
