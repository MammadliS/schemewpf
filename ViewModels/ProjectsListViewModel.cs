﻿using SchemeWpf.Models;
using SchemeWpf.Navigation;
using SchemeWpf.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeWpf.ViewModels
{
    class ProjectsListViewModel : ViewModelBaseExtention
    {
        

        public ProjectsListViewModel(INavigationService navigationService)
            : base(navigationService)
        {


            Projects = new ObservableCollection<Proj>()
            {
                new Proj { Name = "Some project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
                new Proj { Name = "Some other project"},
            };

            CurrentViewModel = new BoardViewModel();

            (CurrentViewModel as BoardViewModel).BoardLists = new ObservableCollection<Models.BoardList>()
            {

                new Models.BoardList() { Name = "ToDo" , Cards = new ObservableCollection<Card>()},
                new Models.BoardList() { Name = "Doing" , Cards = new ObservableCollection<Card>() },
                new Models.BoardList() { Name = "Done" ,  Cards = new ObservableCollection<Card>() }
            };

            for (int i = 0; i < 20; i++)
            {
                (CurrentViewModel as BoardViewModel).BoardLists.FirstOrDefault().Cards.Add(new Card() { Name = $"aadadrtstghreherwhrehrhetytyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy{i}" });
            }
         

            //Messenger.Default.Register<Session>(this, Setsession);
        }

        private ObservableCollection<Proj> projects;
        public ObservableCollection<Proj> Projects
        {
            get => projects;
            set => Set(ref projects, value);
        }

        private object currentViewModel;
        public object CurrentViewModel
        {
            get => currentViewModel;
            set => Set(ref currentViewModel, value);
        }
    }

    class Proj
    {
        public string Name { get; set; }
    }


    

    //private string addToDo;
    //public string AddToDo
    //{
    //    get => addToDo;
    //    set => Set(ref addToDo, value);
    //}

    //private RelayCommand<ToDo> selectedTodoCommand;
    //public RelayCommand<ToDo> SelectedTodoCommand
    //{
    //    get
    //    {
    //        return selectedTodoCommand ?? (selectedTodoCommand = new RelayCommand<ToDo>(
    //            param =>
    //            {
    //                if (param != null)
    //                {
    //                    ChangeStateInCollection(param.Id);
    //                    ChangeStateInDataBase(param.Id);
    //                }
    //            }
    //        ));
    //    }
    //}
}


