﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using SchemeWpf.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeWpf.ViewModels
{
    class BoardViewModel : ViewModelBase
    {

        private Card _selectedCard;

        public Card SelectedCard
        {
            get => _selectedCard;
            set => Set(ref _selectedCard, value);
        }


        private RelayCommand _changeState;
        public RelayCommand ChangeState
        {
            get
            {
                return _changeState ?? (_changeState = new RelayCommand(
                    () =>
                    {
                        MessageBox.Show("dwqdwqfg");
                        SelectedCard.IsEnabled = true;
                    }
                ));
            }
        }


        private ObservableCollection<BoardList> boardLists;
        public ObservableCollection<BoardList> BoardLists
        {
            get => boardLists;
            set { Set(ref boardLists, value);}
        }
    }
}
