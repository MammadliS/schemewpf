﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using SchemeWpf.Navigation;
using SchemeWpf.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SchemeWpf.ViewModels
{
    class LoginViewModel : ViewModelBaseExtention
    {
        public LoginViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            //Messenger.Default.Register<Session>(this, Setsession);
        }

        private RelayCommand loginCommand;
        public RelayCommand LoginCommand
        {
            get
            {
                return loginCommand ?? (loginCommand = new RelayCommand(
                    () =>
                    {
                        navigationService.NavigateTo(VM.ProjectsListViewModel);
                    }
                ));
            }
        }
    }
}
