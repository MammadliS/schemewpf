﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemeWpf.Navigation
{
    enum VM
    {
        LoginViewModel,
        LoadingViewModel,
        ProjectsListViewModel,
        BoardViewModel
    }
}
